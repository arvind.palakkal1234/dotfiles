{pkgs, ...}: {
  environment = {
    enableAllTerminfo = false;
    variables = {
      DIRENV_LOG_FORMAT = "";
      EDITOR = "hx";
    };
    systemPackages = builtins.attrValues {inherit (pkgs) git;};
  };
}
