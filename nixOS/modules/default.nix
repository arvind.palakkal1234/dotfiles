let
  doas = import ./doas.nix;
  environment = import ./environment.nix;
  locale = import ./locale.nix;
  nix = import ./nix.nix;
  plasma = import ./plasma.nix;
  sound = import ./sound.nix;
  steam = import ./steam.nix;
  system = import ./system.nix;
  user = import ./user.nix;
  wayland = ./wayland.nix;
in {
  flake = {
    nixosModules = {
      inherit
        doas
        environment
        locale
        nix
        plasma
        sound
        steam
        system
        user
        wayland
        ;
      base = {
        imports = [
          doas
          environment
          nix
          system
          user
        ];
      };
      graphicalBase = {
        imports = [
          plasma
          sound

          wayland
        ];
      };
      laptop = {
        imports = [
          locale
        ];
      };
    };
  };
}
