{flake, ...}: let
  me = flake.config.people.myself;
in {
  users.users.${me} = {
    openssh.authorizedKeys.keys = ["ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKogc+5HBh1GSsEN1vqTchxRh0ERCJ6PmQnr+X7efhdB isaac@desktop"];
  };
}
