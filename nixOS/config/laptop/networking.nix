{lib, ...}: {
  networking = {
    hostName = "laptop";
    networkmanager.enable = true;
    useDHCP = lib.mkDefault true;
  };
  services.sshd.enable = true;
}
