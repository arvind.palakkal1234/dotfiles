{
  imports = [
    ./boot.nix
    ./file-system.nix
    ./hardware-configuration.nix
    ./networking.nix
    ./ssh.nix
  ];
}
