{
  config,
  lib,
  modulesPath,
  ...
}: {
  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;

  imports = [(modulesPath + "/installer/scan/not-detected.nix")];
}
