{
  fileSystems."/" = {
    device = "/dev/disk/by-uuid/d58bc6e0-6362-4765-b139-b26a74b83578";
    fsType = "ext4";
  };

  fileSystems."/boot/efi" = {
    device = "/dev/disk/by-uuid/8AEE-C4E3";
    fsType = "vfat";
  };
}
