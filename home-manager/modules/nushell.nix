{
  programs.nushell = {
    configFile.text = ''
      sleep 7ms
      $env.config = {show_banner: false}
    '';
    enable = true;
    shellAliases = {
      e = "edgedb";
      h = "hledger";
      la = "ls -la";
      ll = "ls -l";
      lg = "lazygit";
      tb = "nc termbin.com 9999";
      y = "yy";
      yt = "yt-dlp";
      ze = "zellij";
    };
  };
}
