{
  inputs,
  pkgs,
  ...
}: {
  programs.helix = {
    enable = true;
    languages = {
      language = [
        {
          auto-format = true;
          formatter.command = "shfmt";
          name = "bash";
        }
        {
          auto-format = true;
          language-servers = ["elm-language-server"];
          name = "elm";
        }
        {
          auto-format = true;
          name = "haskell";
          roots = ["flake.nix"];
        }
        {
          auto-format = true;
          formatter.command = "alejandra";
          language-servers = ["nil"];
          name = "nix";
        }
        {
          auto-format = true;
          formatter.args = ["-s"];
          formatter.command = "nufmt";
          name = "nu";
          roots = ["flake.nix"];
        }
        {
          auto-format = true;
          name = "rust";
          roots = ["flake.nix"];
        }
        {
          auto-format = true;
          name = "toml";
        }
        {
          auto-format = true;
          formatter.command = "typstfmt";
          name = "typst";
        }
        {
          auto-format = true;
          formatter.args = ["-in"];
          formatter.command = "yamlfmt";
          name = "yaml";
        }
      ];
      language-server = {
        elm-language-server = {
          config.elmLS = {
            disableElmLSDiagnostics = true;
            elmReviewDiagnostics = "warning";
          };
        };
        # gpt = {
        #   command = "helix-gpt";
        # };
      };
    };
    package = inputs.helix.packages.${pkgs.system}.default;
    settings = {
      editor = {
        auto-format = true;
        auto-save = true;
        line-number = "relative";
      };
      keys = {
        normal = {
          space = {
            f = ":format";
            q = ":q";
            w = ":w";
          };
        };
      };
      theme = "catppuccin_mocha";
    };
  };
}
