{
  lib,
  pkgs,
  ...
}: {
  programs = {
    vscode = {
      enable = true;
      extensions = builtins.attrValues {
        inherit (pkgs.vscode-extensions.catppuccin) catppuccin-vsc;
        inherit (pkgs.vscode-extensions.elmtooling) elm-ls-vscode;
        inherit (pkgs.vscode-extensions.foxundermoon) shell-format;
        inherit (pkgs.vscode-extensions.gruntfuggly) todo-tree;
        inherit (pkgs.vscode-extensions.haskell) haskell;
        inherit (pkgs.vscode-extensions.james-yu) latex-workshop;
        inherit (pkgs.vscode-extensions.jnoortheen) nix-ide;
        inherit (pkgs.vscode-extensions.justusadam) language-haskell;
        inherit (pkgs.vscode-extensions.kamadorueda) alejandra;
        inherit (pkgs.vscode-extensions.mattn) lisp;
        inherit (pkgs.vscode-extensions.mhutchie) git-graph;
        inherit (pkgs.vscode-extensions.mkhl) direnv;
        inherit (pkgs.vscode-extensions.ms-vscode) cpptools;
        inherit (pkgs.vscode-extensions.ms-vscode-remote) remote-containers;
        inherit (pkgs.vscode-extensions.nvarner) typst-lsp;
        inherit (pkgs.vscode-extensions.pkief) material-icon-theme;
        inherit (pkgs.vscode-extensions.redhat) vscode-yaml;
        inherit (pkgs.vscode-extensions.tamasfe) even-better-toml;
        inherit (pkgs.vscode-extensions.skellock) just;
        inherit (pkgs.vscode-extensions.streetsidesoftware) code-spell-checker;
        inherit (pkgs.vscode-extensions.xaver) clang-format;
      };
      userSettings = {
        "C_Cpp.clang_format_path" = "/nix/store/mlw1s9ysda190s6zzs6v9n65krqvjq7f-clang-11.1.0/bin/clang-format";
        "[c]"."editor.defaultFormatter" = "xaver.clang-format";
        "[cpp]"."editor.defaultFormatter" = "xaver.clang-format";
        "[javascript]"."editor.defaultFormatter" = "vscode.typescript-language-features";
        "[typescript]"."editor.defaultFormatter" = "vscode.typescript-language-features";
        "[typst]"."editor.defaultFormatter" = "nvarner.typst-lsp";
        "editor.fontFamily" = "";
        "editor.insertSpaces" = false;
        "elmLS.disableElmLSDiagnostics" = true;
        "elmLS.elmReviewDiagnostics" = "warning";
        "files.autoSave" = "afterDelay";
        "git.confirmSync" = false;
        "git.enableSmartCommit" = true;
        "nix.enableLanguageServer" = true;
        "git.openRepositoryInParentFolders" = "never";
        "nix.serverPath" = lib.getExe pkgs.nil;
        "nix.formatterPath" = lib.getExe pkgs.alejandra;
        "typst-lsp.experimentalFormatterMode" = "on";
        "window.menuBarVisibility" = "toggle";
        "workbench.colorTheme" = "Catppuccin Mocha";
        "workbench.iconTheme" = "material-icon-theme";
        "workbench.startupEditor" = "none";
      };
    };
  };
}
