{
  inputs,
  pkgs,
  ...
}: let
  stable = inputs.nixpkgs-stable.legacyPackages.${pkgs.system};
in {
  home.packages = builtins.attrValues {
    inherit
      (pkgs)
      inkscape
      libreoffice
      ;
    inherit
      (stable)
      gimp-with-plugins
      ; # These randomly broke, version it back up whenever you find yourself back in this file.
  };
}
