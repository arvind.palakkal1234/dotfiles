{pkgs, ...}: {
  home.packages = builtins.attrValues {
    inherit (pkgs) okular;
    inherit (pkgs.xfce) thunar;
  };
}
