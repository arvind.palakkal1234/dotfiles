{pkgs, ...}: {
  home.packages = builtins.attrValues {
    inherit
      (pkgs)
      discord
      xdg-utils # Currently needed to open links from Discord.
      ;
  };
}
