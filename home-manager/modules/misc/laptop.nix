{pkgs, ...}: {
  home.packages = builtins.attrValues {
    inherit
      (pkgs)
      brave
      wireshark
      teams-for-linux
      ;
  };
}
