{pkgs, ...}: {
  home.packages = builtins.attrValues {
    inherit
      (pkgs)
      speedcrunch
      obsidian
      ;
  };
}
