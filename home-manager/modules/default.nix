let
  brave = import ./brave.nix;
  bottom = import ./bottom.nix;
  direnv = import ./direnv.nix;
  firefox = import ./firefox;
  git = import ./git.nix;
  helix = import ./helix.nix;
  home-manager = import ./home-manager.nix;
  kitty = import ./kitty.nix;
  lazygit = import ./lazygit.nix;
  misc-commandLine = import ./misc/command-line.nix;
  misc-communication = import ./misc/communication.nix;
  misc-laptop = import ./misc/laptop.nix;
  misc-editing = import ./misc/editing.nix;
  misc-finance = import ./misc/finance.nix;
  misc-gaming = import ./misc/gaming.nix;
  misc-graphical-base = import ./misc/graphical-base.nix;
  misc-media = import ./misc/media.nix;
  misc-productivity = import ./misc/productivity.nix;
  mpv = import ./mpv.nix;
  nushell = import ./nushell.nix;
  obs = import ./obs;
  qbittorrent = import ./qbittorrent;
  starship = import ./starship.nix;
  vscodium = import ./vscodium.nix;
  yazi = import ./yazi.nix;
  yt-dlp = import ./yt-dlp.nix;
  zellij = import ./zellij.nix;
  zoxide = import ./zoxide.nix;
in {
  flake.homeModules = {
    inherit
      brave
      bottom
      direnv
      firefox
      git
      helix
      home-manager
      kitty
      lazygit
      misc-commandLine
      misc-communication
      misc-laptop
      misc-editing
      misc-finance
      misc-gaming
      misc-graphical-base
      misc-media
      misc-productivity
      mpv
      nushell
      obs
      qbittorrent
      starship
      vscodium
      yazi
      yt-dlp
      zellij
      zoxide
      ;
    commandline = {
      imports = [
        bottom
        direnv
        git
        helix
        home-manager
        kitty
        lazygit
        misc-commandLine
        nushell
        starship
        yazi
        yt-dlp
        zellij
        zoxide
      ];
    };
    communication = {
      imports = [
        misc-communication
      ];
    };
    editing = {imports = [misc-editing];};
    finance = {imports = [misc-finance];};
    gaming = {imports = [misc-gaming];};
    graphicalBase = {
      imports = [
        misc-graphical-base
      ];
    };
    internet = {
      imports = [
        brave
        firefox
      ];
    };
    laptop = {
      imports = [
        misc-laptop
      ];
    };
    media = {
      imports = [
        misc-media
        mpv
        obs
        qbittorrent
      ];
    };
    programming = {imports = [vscodium];};
    productivity = {imports = [misc-productivity];};
    services = {imports = [];};
  };
}
