{
  flake,
  stateVersion,
  ...
}: let
  inherit (flake.config.people) myself;
in {
  home = {
    homeDirectory = "/home/${myself}";
    inherit stateVersion;
    sessionVariables = {};
    username = myself;
  };
}
