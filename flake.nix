{
  description = "Hydra";

  inputs = {
    catppuccin.url = "github:catppuccin/nix";
    helix.url = "github:helix-editor/helix";
    home-manager.url = "github:nix-community/home-manager";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    nixpkgs-stable.url = "github:NixOS/nixpkgs/nixos-23.11";
    pre-commit-hooks-nix.url = "github:cachix/pre-commit-hooks.nix";
  };

  outputs = inputs @ {
    flake-parts,
    self,
    ...
  }: let
    stateVersion = "22.11";
    system = "x86_64-linux";
  in
    flake-parts.lib.mkFlake {inherit inputs;} {
      imports = [
        inputs.pre-commit-hooks-nix.flakeModule
        ./lib
        ./parts
        ./home-manager/modules
        ./nixOS/modules
        ./users
        ./themes
      ];
      flake = {config, ...}: {
        homeConfigurations = {
          laptop =
            self.lib.mkHome
            [
              ./home-manager
              config.homeModules.commandline
              config.homeModules.communication
              config.homeModules.graphicalBase
              config.homeModules.internet
              config.homeModules.laptop
              config.homeModules.media
              config.homeModules.programming
              config.homeModules.productivity
              config.homeModules.services
              inputs.catppuccin.homeManagerModules.catppuccin
            ]
            stateVersion
            system;
        };
        nixosConfigurations = {
          laptop =
            self.lib.mkLinuxSystem
            [
              ./nixOS/config/laptop
              config.nixosModules.base
              config.nixosModules.graphicalBase
              config.nixosModules.laptop
            ]
            stateVersion
            system;
        };

        templates = {
          c = {
            path = ./templates/c;
            description = "C Environment";
          };
        };
      };
      systems = [system];
    };
}
