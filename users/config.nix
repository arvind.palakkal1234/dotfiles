let
  myself = "arvind";
in {
  inherit myself;
  users = {
    "${myself}" = {
      name = "Arvind Palakkal";
      email = "arvind.palakkal1234@gmail.com";
      sshKeys = ["ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKLNm4PsZPkegFVPL1spAIY5RUWDhplFSQF7f7UpIAwj arvind@laptop"];
    };
  };
}
