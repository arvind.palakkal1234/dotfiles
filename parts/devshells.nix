{
  pkgs,
  config,
  ...
}: {
  devShells = {
    default = pkgs.mkShell {
      packages = builtins.attrValues {
        inherit
          (pkgs)
          alejandra
          dhall
          dhall-json
          dhall-lsp-server
          gource
          just
          nil
          sops
          ssh-to-age
          vscode-langservers-extracted
          ;
      };

      shellHook = "${config.pre-commit.installationScript}";
    };
  };
}
