{
  pre-commit.settings.hooks = {
    alejandra.enable = true;
    convco.enable = true;
    statix.enable = true;
  };
}
